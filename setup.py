from setuptools import setup, find_packages


with open('README.md', encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='csv',
    version='0.1.0',
    description='Component for csv data.',
    long_description=long_description,
    url='https://gitlab.com/zilleanai/comp/csv',
    author='chriamue',
    license='Not open source',

    # https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Flask',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    packages=find_packages(exclude=['docs', 'tests']),
    install_requires=[
        'flask-unchained>=0.7.6',
    ],
    extras_require={
        'dev': [
            'coverage',
            'pytest',
            'pytest-flask',
            'tox',
        ],
    },
    include_package_data=True,
    zip_safe=False,
)
