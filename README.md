# csv

Component for csv data.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/csv
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.csv',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.csv.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  Csv,
  LabelCsv,
  Csvs
} from 'comps/csv/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  Csv: 'Csv',
  LabelCsv: 'LabelCsv',
  Csvs: 'Csvs',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.Csv,
    path: '/csv/:id',
    component: Csv,
  },
  {
    key: ROUTES.LabelCsv,
    path: '/labelcsv/:id',
    component: LabelCsv,
  },
  {
    key: ROUTES.Csvs,
    path: '/csvs',
    component: Csvs,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.Csvs} />
    ...
</div>
```
