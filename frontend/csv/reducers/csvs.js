import { listCsvs } from 'comps/csv/actions'


export const KEY = 'csvs'

const initialState = {
  isLoading: false,
  isLoaded: false,
  paths: [],
  byPath: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { csvs } = payload || {}
  const { paths, byPath } = state

  switch (type) {
    case listCsvs.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case listCsvs.SUCCESS:
      const path = csvs.project + (csvs.path == '' ? '/' : '/' + csvs.path)
      if (!paths.includes(path)) {
        paths.push(path)
      }
      byPath[path] = csvs.files
      return {
        ...state,
        csvs: csvs.files,
        paths,
        byPath,
        isLoaded: true,
      }

    case listCsvs.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case listCsvs.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectCsvs = (state) => state[KEY]
export const selectCsvsList = (state, project, path) => {
  const p = project + path
  return selectCsvs(state).byPath[p]
}
