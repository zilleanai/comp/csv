import { loadCSVData } from 'comps/csv/actions'


export const KEY = 'csv_data'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { csv } = payload || {}
  const { ids, byId } = state
  switch (type) {
    case loadCSVData.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case loadCSVData.SUCCESS:
      const id = csv.id
      if (!ids.includes(id)) {
        ids.push(id)
      }
      byId[id] = csv
      return {
        ...state,
        ids,
        byId,
      }

    case loadCSVData.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectCSVData = (state) => state[KEY]
export const selectCSVDataById = (state, id) => selectCSVData(state).byId[id]
