import { loadRecordData } from 'comps/csv/actions'


export const KEY = 'record_csv_data'

const initialState = {
  isLoading: false,
  ids: [],
  byId: {},
  error: null,
}

export default function (state = initialState, action) {
  const { type, payload } = action
  const { id } = payload || {}
  const { ids, byId } = state

  switch (type) {
    case loadRecordData.REQUEST:
      return {
        ...state,
        isLoading: true,
      }

    case loadRecordData.SUCCESS:
      if (!ids.includes(id)) {

        ids.push(id)
      }
      byId[id] = payload
      return {
        ...state,
        ids,
        byId,
      }

    case loadRecordData.FAILURE:
      return {
        ...state,
        error: payload.error,
      }

    case loadRecordData.FULFILL:
      return {
        ...state,
        isLoading: false,
      }

    default:
      return state
  }
}

export const selectRecordData = (state) => state[KEY]
export const selectRecordDataById = (state, id) => selectRecordData(state).byId[id]
