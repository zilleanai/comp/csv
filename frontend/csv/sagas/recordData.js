import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadRecordData } from 'comps/csv/actions'
import CSVApi from 'comps/csv/api'
import { selectRecordData } from 'comps/csv/reducers/recordData'


export const KEY = 'recordData'

export const maybeLoadRecordDataSaga = function* ({payload}) {
  const { byId, isLoading } = yield select(selectRecordData)
  const {id} = payload
  const isLoaded = !!byId[id]
  if (!(isLoaded || isLoading)) {
    yield put(loadRecordData.trigger({ payload }))
  }
}

export const loadRecordDataSaga = createRoutineSaga(
  loadRecordData,
  function* successGenerator({payload}) {
    let csv = yield call(CSVApi.loadRecordData, payload)
    yield put(loadRecordData.success(csv))
  }
)

export default () => [
  takeEvery(loadRecordData.MAYBE_TRIGGER, maybeLoadRecordDataSaga),
  takeLatest(loadRecordData.TRIGGER, loadRecordDataSaga),
]
