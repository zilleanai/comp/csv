import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { listCsvs } from '../actions'
import CsvApi from '../api'
import { selectCsvs } from '../reducers/csvs'


export const KEY = 'csvs'

export const maybeListCsvsSaga = function* (payload) {
  const { byPath, isLoading } = yield select(selectCsvs)
  const path = payload.platform + payload.path
  const isLoaded = !!byPath[path]
  if (!(isLoaded || isLoading)) {
    yield put(listCsvs.trigger(payload))
  }
}

export const listCsvsSaga = createRoutineSaga(
  listCsvs,
  function* successGenerator({ payload: payload }) {
    const csvs = yield call(CsvApi.listCsvs, payload)
    yield put(listCsvs.success({
      csvs,
    }))
  },
)

export default () => [
  takeEvery(listCsvs.MAYBE_TRIGGER, maybeListCsvsSaga),
  takeLatest(listCsvs.TRIGGER, listCsvsSaga),
]
