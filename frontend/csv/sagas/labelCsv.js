import { call, put, takeLatest } from 'redux-saga/effects'

import { labelCsv } from 'comps/csv/actions'
import { createRoutineSaga } from 'sagas'
import CsvApi from 'comps/csv/api'


export const KEY = 'labelCsv'

export const labelCsvSaga = createRoutineSaga(
  labelCsv,
  function *successGenerator(payload) {
    payload = { id: payload.id, 'positive': payload.positive, 'negative': payload.negative }
    const response = yield call(CsvApi.labelCsv, payload)
    yield put(labelRecord.success(response))}
)

export default () => [
  takeLatest(labelCsv.TRIGGER, labelCsvSaga),
]
