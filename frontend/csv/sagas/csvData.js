import { call, put, select, takeEvery, takeLatest } from 'redux-saga/effects'

import { createRoutineSaga } from 'sagas'
import { convertDates } from 'utils'

import { loadCSVData } from 'comps/csv/actions'
import CSVApi from 'comps/csv/api'
import { selectCSVData } from 'comps/csv/reducers/csvData'


export const KEY = 'csvData'

export const maybeLoadCSVDataSaga = function* ({payload}) {
  const { byId, isLoading } = yield select(selectCSVData)
  const isLoaded = !!byId[payload.id]
  if (!(isLoaded || isLoading)) {
    yield put(loadCSVData.trigger({payload}))
  }
}

export const loadCSVDataSaga = createRoutineSaga(
  loadCSVData,
  function* successGenerator({ payload }) {
    let csv = yield call(CSVApi.loadCSVData, payload)
    yield put(loadCSVData.success({ csv }))
  }
)

export default () => [
  takeEvery(loadCSVData.MAYBE_TRIGGER, maybeLoadCSVDataSaga),
  takeLatest(loadCSVData.TRIGGER, loadCSVDataSaga),
]
