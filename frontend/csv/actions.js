import { createRoutine } from 'actions'
export const listCsvs = createRoutine('csv/LIST_CSVS')
export const download = createRoutine('csv/DOWNLOAD_CSV')
export const loadCSVData = createRoutine('csv/LOAD_CSV_DATA')
export const labelCsv = createRoutine('csv/LABEL_CSV')
export const loadRecordData = createRoutine('csv/LOAD_RECORD_DATA')