import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { PageContent } from 'components'
import { CsvList, Dir } from 'comps/csv/components'
import { DirList } from 'comps/file/components'

class Csvs extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.csvs-page',
          intro: 'CSV files on this page.',
        },
        {
          element: '.csv-list',
          intro: 'List of csv files.',
        },
      ],
    }
  }

  dirDecorator = (dir, path, i) => {
    return (
      <li key={i}>
        <Dir path={path} dir={dir} />
      </li>
    )
  }

  render() {
    const { subpath } = this.props
    const { stepsEnabled, steps, initialStep } = this.state
    return (
      <PageContent className='csvs-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Csvs</title>
        </Helmet>
        <h1>Csvs</h1>
        <DirList path={subpath} decorator={this.dirDecorator} />
        <CsvList path={subpath} />
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const subpath = decodeURIComponent(props.match.params.subpath)
    return {
      subpath
    }
  }
)


export default compose(
  withConnect
)(Csvs)
