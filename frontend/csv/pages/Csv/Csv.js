import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Select from 'react-select';
import Helmet from 'react-helmet'

import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { NavLink, PageContent } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { ROUTES } from 'routes'
import { v1 } from 'api'

import PivotTableUI from 'react-pivottable/PivotTableUI';
import 'react-pivottable/pivottable.css';
import TableRenderers from 'react-pivottable/TableRenderers';
import Plot from 'react-plotly.js';
import createPlotlyRenderers from 'react-pivottable/PlotlyRenderers';

import { storage } from 'comps/project'
import { loadCSVData } from 'comps/csv/actions'
import { newTag } from 'comps/tag/actions'
import { selectCSVDataById } from 'comps/csv/reducers/csvData'

import './csv.scss'
const PlotlyRenderers = createPlotlyRenderers(Plot);

class Csv extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stepsEnabled: true,
      initialStep: 0,
      selectedTags:null,
      steps: [
        {
          element: '.csv-page',
          intro: 'CSV files are tables of data.',
        },
        {
          element: '.csv-id',
          intro: 'This is the filename of current csv file.',
        },
        {
          element: '.csv-table',
          intro: 'Data of the csv file can be viewed in the table.',
        },
        {
          element: '.tags-group',
          intro: 'Select columns from the table and add them to the Tags.',
        },
        {
          element: '.csv-label',
          intro: 'Labels for table rows can be added here.',
        },
        {
          element: '.csv-download-excel',
          intro: 'Current csv can be downloaded as excel file.',
        },
        {
          element: '.csv-download',
          intro: 'Current csv can be downloaded as csv file.',
        },
      ],
    }
  }

  componentWillMount() {
    const { loadCSVData, id } = this.props
    loadCSVData.maybeTrigger({ id })
  }

  addColumnsToTags = () => {
    const {selectedTags} = this.state;
    for (var key in selectedTags) {
      const tag = selectedTags[key].value
      this.props.newTag.trigger({name:tag})
    }
    this.setState({ selectedTags: null });
  }

  toOptions(list) {
    var options = []
    list.forEach(function (value, key) {
      options.push({ value: value.name, label: value.name })
    }, list)
    return options
  }

  handleTagsChange = (selectedOption) => {
    this.setState({ selectedTags: selectedOption });
  }

  render() {
    const { id, csv, isLoaded, error } = this.props
    if (!isLoaded) {
      return null
    }
    const { stepsEnabled, steps, initialStep, selectedTags } = this.state
    const csv_obj = JSON.parse(csv.data)
    const {data, schema} = csv_obj
    return (
      <PageContent className='csv-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Csv {id} </title>
        </Helmet>
        <h5 className='csv-id'>CSV ID: {id}</h5>
        <div className="tags-group">
          <Select
          value={selectedTags}
          onChange={this.handleTagsChange}
          name="tags"
          options={this.toOptions(schema.fields)}
          isMulti
          className="basic-multi-select"
          classNamePrefix="select"
        />
        <button type='button' onClick={this.addColumnsToTags}>Add column names to tags</button>
        </div>
        <div className='csv-table'>
          <PivotTableUI
            data={data}
            onChange={s => this.setState(s)}
            renderers={Object.assign({}, TableRenderers, PlotlyRenderers)}
            {...this.state}
          />
        </div>
        <NavLink className='csv-label' to={ROUTES.LabelCsv} params={{ id: id }}>
          <button>Label</button>
        </NavLink>
        <a className='csv-download-excel' href={v1(`/csv/excel/${storage.getProject()}${id}`)} download={`${id}.xlsx`}><button>Download as Excel file</button></a>
        <a className='csv-download' href={v1(`/csv/download/${storage.getProject()}${id}`)} download={`${id}.csv`}><button>Download as CSV file</button></a>
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const id = decodeURIComponent(props.match.params.id)
    const csv = selectCSVDataById(state, id)
    return {
      id,
      csv,
      isLoaded: !!csv,
    }
  },
  (dispatch) => bindRoutineCreators({ loadCSVData, newTag }, dispatch),
)

const withReducer = injectReducer(require('comps/csv/reducers/csvData'))
const withSaga = injectSagas(require('comps/csv/sagas/csvData'))

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Csv)
