import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import Helmet from 'react-helmet'

import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { PageContent, CheckboxList } from 'components'
import { Steps } from 'intro.js-react';
import 'intro.js/introjs.css';
import { v1 } from 'api'
import classnames from 'classnames'

import { List } from 'immutable';
import { Button, ButtonGroup } from 'react-bootstrap';

import Plot from 'react-plotly.js';
import { storage } from 'comps/project'
import { labelCsv, loadRecordData } from 'comps/csv/actions'
import { selectRecordDataById } from 'comps/csv/reducers/recordData'

import './label-csv.scss'

class LabelCsv extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      action: '1',
      positivePoints: {
        x: List(),
        y: List(),
        index: List()
      },
      negativePoints: {
        x: List(),
        y: List(),
        index: List()
      },
      stepsEnabled: true,
      initialStep: 0,
      steps: [
        {
          element: '.label-page',
          intro: 'This page allows to label data on a graph.',
        },
        {
          element: '.label-positive',
          intro: 'Data labeled as positive will be saved as 1 in column action',
        },
        {
          element: '.label-negative',
          intro: 'Data labeled as negative will be saved as -1 in column action',
        },
        {
          element: '.label-features',
          intro: 'Data selected here will be visualized in the graph.',
        },
        {
          element: '.label-plot',
          intro: 'Data is plottet in this graph.',
        },
        {
          element: '.button-primary',
          intro: 'Labeled data can be saved pressing the button.',
        },
      ],
    }
    this.buttonHandle = this.buttonHandle.bind(this)
  }

  loadActionPoints = (data) => {
    var positivePoints = {
      x: List(),
      y: List(),
      index: List()
    }
    var negativePoints = {
      x: List(),
      y: List(),
      index: List()
    }
    if (data.action) {
      for (var i = 0; i < data.action.length; i++) {
        var a = data.action[i]
        if (a === 1) {
          var x = data.time[i]
          var y = data.price[i]
          positivePoints.x = positivePoints.x.push(x)
          positivePoints.y = positivePoints.y.push(y)
          positivePoints.index = positivePoints.index.push(i)
        } else if (a === -1) {
          var x = data.time[i]
          var y = data.price[i]
          negativePoints.x = negativePoints.x.push(x)
          negativePoints.y = negativePoints.y.push(y)
          negativePoints.index = negativePoints.index.push(i)
        }
      }
    }

    this.setState({
      positivePoints: positivePoints,
      negativePoints: negativePoints,
    });
  }

  plotly_click = (data) => {
    // source: https://codepen.io/robowen5mac/pen/GxgZJE?editors=0010
    let x = data.points[0].x;
    let y = data.points[0].y;
    let index = data.points[0].pointIndex;

    let newpositivePoints;
    let newnegativePoints;
    let { action, positivePoints, negativePoints } = this.state;

    if (action === '1') {
      newnegativePoints = negativePoints
      let selectedIndex = positivePoints.x.indexOf(x);
      if (selectedIndex === -1) {
        newpositivePoints = {
          x: positivePoints.x.push(x),
          y: positivePoints.y.push(y),
        };
      } else {
        newpositivePoints = {
          x: positivePoints.x.delete(selectedIndex),
          y: positivePoints.y.delete(selectedIndex),
        };
      }
      this.setState({
        positivePoints: newpositivePoints
      });
    } else if (action === '-1') {
      newpositivePoints = positivePoints
      let selectedIndex = negativePoints.x.indexOf(x);
      if (selectedIndex === -1) {
        newnegativePoints = {
          x: negativePoints.x.push(x),
          y: negativePoints.y.push(y),
        };
      } else {
        newpositivePoints = {
          x: negativePoints.x.delete(selectedIndex),
          y: negativePoints.y.delete(selectedIndex),
        };
      }
      this.setState({
        negativePoints: newnegativePoints
      });
    }
  }


  handleFeatures = (features) => {
    this.setState({ features: features });
  }

  buttonHandle(event) {
    this.setState({ action: event.target.attributes.getNamedItem('data-key').value })
  }

  dataToIndices(data, points) {
    let indices = [];
    for (var i = 0; i < data.time.length; i++) {
      for (var j = 0; j < points.x.size; j++) {
        if (points.x.get(j) == data.time[i]) {
          indices.push(i)
        }
      }
    }
    return indices
  }

  plotData(features, data) {
    let result =
      [
        {
          type: "scatter",
          mode: "markers",
          x: this.state.positivePoints.x.toJS(),
          y: this.state.positivePoints.y.toJS(),
          marker: { color: "green", size: 8 },
          name: 'positive'
        },
        {
          type: "scatter",
          mode: "markers",
          x: this.state.negativePoints.x.toJS(),
          y: this.state.negativePoints.y.toJS(),
          marker: { color: "red", size: 8 },
          name: 'negative'
        }
      ];
    if (features && features.length > 0) {
      let feature_data = features.map((feature, i) => {
        var d = {
          x: data.time,
          y: data[feature],
          type: 'scatter',
          mode: 'lines+points',
          marker: { color: 'blue' },
          name: feature
        }
        return d
      })
      return [...feature_data, ...result]
    }
    return [...result]
  }

  componentWillMount() {
    const { loadRecordData, id } = this.props
    loadRecordData.maybeTrigger({ id })
  }

  componentWillReceiveProps(nextProps) {
    const { loadRecordData, id } = nextProps
    if (id != this.props.id) {
      loadRecordData.maybeTrigger({ id })
    }
    if (nextProps.isLoaded) {
      this.loadActionPoints(nextProps.csv.data)
    }
  }

  render() {
    const { id, csv, isLoaded, error, pristine, submitting } = this.props
    if (!isLoaded) {
      return null
    }
    const { stepsEnabled, steps, initialStep } = this.state
    var data = (csv.data)
    if (typeof data === 'string') {
      data = JSON.parse(data)
    }
    return (
      <PageContent className='label-page'>
        <Steps
          enabled={stepsEnabled}
          steps={steps}
          initialStep={initialStep}
          onExit={() => {
            this.setState(() => ({ stepsEnabled: false }));
          }}
        />
        <Helmet>
          <title>Csv {id} </title>
        </Helmet>
        <div className="csv-plot">
          <ButtonGroup onClick={(e) => this.buttonHandle(e)}>
            <Button className='label-positive' data-key='1' >Positive</Button>

            <Button className='label-negative' data-key='-1'>Negative</Button>

          </ButtonGroup>
          <div className='label-features'>
            <CheckboxList items={Object.keys(data).filter(e => e !== 'index' && e !== 'action' && e !== 'time')}
              onSelectItems={this.handleFeatures} ></CheckboxList>
          </div>
          <div className='label-plot'>
            <Plot
              data={this.plotData(this.state.features, data)}
              layout={{ title: id }}
              onClick={this.plotly_click}
            />
          </div>
          <div className="row">
            <button type="submit"
              className="button-primary"
              onClick={(e) => {
                this.props.labelCsv.trigger({
                  'id': id, 'positive': this.dataToIndices(data, this.state.positivePoints),
                  'negative': this.dataToIndices(data, this.state.negativePoints)
                })
              }}
              disabled={pristine || submitting}
            >
              {submitting ? 'Recording...' : 'Save Labels'}
            </button>
          </div>
        </div>
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const id = decodeURIComponent(props.match.params.id)
    const csv = selectRecordDataById(state, id)
    return {
      id,
      csv,
      isLoaded: !!csv,
    }
  },
  (dispatch) => bindRoutineCreators({ labelCsv, loadRecordData }, dispatch),
)

const withReducer = injectReducer(require('comps/csv/reducers/recordData'))
const withSaga = injectSagas(require('comps/csv/sagas/recordData'))
const withSaga2 = injectSagas(require('comps/csv/sagas/labelCsv'))

export default compose(
  withReducer,
  withSaga,
  withSaga2,
  withConnect,
)(LabelCsv)
