import { get, post } from 'utils/request'
import { storage } from 'comps/project'
import { v1 } from 'api'

function csv(uri) {
    return v1(`/csv${uri}`)
}

export default class Project {
    static listCsvs(payload) {
        return get(csv(`/list/${storage.getProject()}${payload.path}`))
    }
    /**
    * @param {Object} project
    */
    static download(id) {
        return get(csv(`/download/${id}`))
    }

    /**
    * @param {Object} csv
    */
    static loadCSVData({id}) {
        return get(csv(`/json/${storage.getProject()}${id}?orient=table`))
    }

    static async loadRecordData({id}) {
        return get(csv(`/dict/${storage.getProject()}${id}`))
    }
    
    static labelCsv({ id, positive, negative }) {
        return post(csv(`/label/${storage.getProject()}${id}`), { id, positive, negative })
    }
}