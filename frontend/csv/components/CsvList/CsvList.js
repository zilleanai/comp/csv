import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import classnames from 'classnames'
import { NavLink } from 'components'
import { ROUTES } from 'routes'

import { storage } from 'comps/project'
import { bindRoutineCreators } from 'actions'
import { injectReducer, injectSagas } from 'utils/async'
import { listCsvs } from 'comps/csv/actions'
import { selectCsvsList } from 'comps/csv/reducers/csvs'
import './csv-list.scss'

class CsvList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    const { path, listCsvs } = this.props
    listCsvs.maybeTrigger({
      project: storage.getProject(), path
    })
  }

  componentWillReceiveProps(nextProps) {
    const { path, listCsvs } = nextProps
    listCsvs.maybeTrigger({
      project: storage.getProject(), path
    })
  }

  render() {
    const { path, csvs } = this.props
    if (!csvs || csvs.length === 0) {
      return (<div>No csvs.</div>)
    }
    return (
      <div className="csv-list" >
        <ul >
          <div>
            {csvs.map((csv, i) => {
              return (
                <li key={i}>
                  <NavLink to={ROUTES.Csv} params={{ id: (path == '/' ? '/'+csv : path+'/'+csv) }}>
                    {csv}
                  </NavLink>
                </li>
              )
            }
            )}
          </div>
        </ul>
      </div>
    )
  }
}

const withReducer = injectReducer(require('comps/csv/reducers/csvs'))
const withSaga = injectSagas(require('comps/csv/sagas/csvs'))

const withConnect = connect(
  (state, props) => {
    const csvs = selectCsvsList(state, storage.getProject(), props.path)
    return {
      csvs
    }
  },
  (dispatch) => bindRoutineCreators({ listCsvs }, dispatch),
)

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(CsvList)
