import React from 'react'
import { compose } from 'redux'
import { v1 } from 'api'
import { NavLink } from 'components'
import { ROUTES } from 'routes'
import { storage } from 'comps/project'
import './dir.scss'

class Dir extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { dir, path } = this.props
    if (!dir || !path) {
      return null
    }
    const subpath = { subpath: (path == '/' ? '' : path) + '/' + dir }
    return (
      <div className='dir'>
        <NavLink to={ROUTES.Csvs} params={subpath}>
          {dir}
        </NavLink>
      </div>
    )
  }
}

export default compose(
)(Dir)
