class Config:
    pass


class DevConfig:
    pass


class ProdConfig:
    pass


class StagingConfig(ProdConfig):
    pass


class TestConfig:
    TESTING = True
