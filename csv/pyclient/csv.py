import requests
from io import BytesIO
import pandas as pd


class csv():
    def __init__(self, project={'api_root': ''}):
        self.project = project

    def list(self, path: str = ''):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'csv/list/' + \
            str(self.project['name']) + path
        return requests.get(url).json()['files']

    def download(self, file_name: str):
        if not file_name.startswith('/'):
            file_name = '/'+file_name
        url = self.project['api_root'] + 'csv/download/' + \
            str(self.project['name']) + file_name
        response = requests.get(url)
        df = pd.read_csv(BytesIO(response.content))
        return df
