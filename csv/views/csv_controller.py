# source: https://stackoverflow.com/questions/23718236/python-flask-browsing-through-directory-with-files
# https://stackoverflow.com/questions/27337013/how-to-send-zip-files-in-the-python-flask-framework

import os
import json
from flask import Response, current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route
from io import StringIO, BytesIO
import zipfile
import pandas as pd

class CsvController(Controller):

    @route('/list/<string:project>/', defaults={'req_path': ''})
    @route('/list/<string:project>/<path:req_path>')
    def csv_dir_listing(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER

        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            resp = jsonify(project=project,
                           path=req_path)
        else:
            # Show directory contents
            files = [f for f in os.listdir(abs_path) if f.endswith(
                '.csv') or f.endswith('.gz')]
            resp = jsonify(project=project,
                           path=req_path, files=files)
        return resp

    # @route('/download/<string:project>/', defaults={'req_path': ''})
    @route('/download/<string:project>/<path:req_path>')
    def download(self, project, req_path):
        print(req_path)
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER

        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            if abs_path.endswith('.csv') or abs_path.endswith('.gz'):
                df = pd.read_csv(abs_path)
                resp = make_response(df.to_csv(index=False))
                resp.headers["Content-Disposition"] = "attachment; filename=" + \
                    os.path.basename(abs_path) + ".csv"
                resp.headers["Content-Type"] = "text/csv"
                return resp
        return abort(404)

    # @route('/json/<string:project>/', defaults={'req_path': ''})
    @route('/json/<string:project>/<path:req_path>')
    def csv_json(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER
        orient = request.args.get('orient')
        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            if abs_path.endswith('.csv') or abs_path.endswith('.gz'):
                df = pd.read_csv(abs_path)
                data = df.reset_index().to_dict(orient='list')
                if orient:
                    data = df.reset_index().to_json(orient=orient)
                return jsonify({'id': '/'+req_path, 'filename': req_path, 'data': data})

        return abort(404)

    @route('/dict/<string:project>/<path:req_path>')
    def csv_dict(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER
        orient = request.args.get('orient') or 'list'
        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            if abs_path.endswith('.csv') or abs_path.endswith('.gz'):
                df = pd.read_csv(abs_path).fillna(0)

                df = df.reset_index()
                data = df.to_dict(orient=orient)
                return jsonify({'id': '/'+req_path, 'filename': req_path, 'data': data})

        return abort(404)

    @route('/excel/<string:project>/<path:req_path>')
    def excel(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER
        # Joining the base and the requested path
        abs_path = os.path.join(BASE_DIR, project, req_path)

        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            if abs_path.endswith('.csv') or abs_path.endswith('.gz'):
                df = pd.read_csv(abs_path)
                data = BytesIO()
                with pd.ExcelWriter(data, engine='xlsxwriter') as writer:
                    df.to_excel(excel_writer=writer, index=False)
                    writer.save()
                    data.seek(0)
                return send_file(data, attachment_filename=os.path.basename(abs_path)+'.xlsx', as_attachment=True)

        return abort(404)

    @route('/label/<string:project>/<path:req_path>', methods=['POST'])
    def label(self, project, req_path):
        BASE_DIR = BundleConfig.current_app.config.DATA_FOLDER
        abs_path = os.path.join(BASE_DIR, project, req_path)

        if not os.path.exists(abs_path):
            return abort(404)

        pos_action = request.json['positive']
        neg_action = request.json['negative']
        filename = abs_path
        df = pd.read_csv(filename, index_col=0)
        df['action'] = 0
        df['action'][pos_action] = 1
        df['action'][neg_action] = -1
        df.to_csv(filename, compression='gzip', index=False)
        resp = jsonify(success=True)
        return resp
